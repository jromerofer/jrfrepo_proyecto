//GESTION DE CUENTAS//

/***********************************************************************************/

/** account
account_id
IBAN
balance
alias
entry_date
**/

//CONSTANTES DE TRBAJO
  const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujrf6ed/collections/";
  const mLabAPIKey = "apiKey=cKW1_6BGSGLHFUmz0If5HlyuBKRT6F32";
  const requestJson = require('request-json');
//Variables de fecha
  var fecha = new Date();
  var aaaa = fecha.getFullYear();
  var mes1 = (fecha.getMonth()+1); //Enero es el 0
  var mes2 = parseInt(mes1,10);
      if (mes2 < 10) {
      var cerito = "0";
      var mes = cerito.concat(mes1);
      } else {
        var mes = mes1;
      }
  var dia = fecha.getDate();
  var hora = fecha.getHours();
  var min = fecha.getMinutes();
  var entryDate = (aaaa + "/" + mes + "/" + dia + " " + hora + ":" + min);


//GET ACCOUNT
function getAccounts (req, res) {
  console.log("GET /apitechu/v2/acounts");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Buscando cuentas");
  httpClient.get("account?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response =!err ? body : {
        "msg" : "Error obteniendo cuentas"
      }
      res.send(response);
    }
  );
}

//POST CREATE ACCOUNT
function createAccounts (req, res) {
  //console.log("IBAN es " + req.body.IBAN);
  //console.log("balance es " + req.body.balance);
  //console.log("account_id es " + req.body.account_id);
  console.log("account_id es " + "GENERADO BY f(x)RANDOM");
  console.log("balance inicial es " + 100);
  console.log("alias es " + req.body.alias);
  console.log("user_id es " + req.body.user_id);


  function randomFromInterval(min,max) {
    return Math.floor(Math.random() * (max-min+1)+min);
  }

  var accountIdRand = randomFromInterval(1000, 9999);
  var ibanRand = randomFromInterval(1000, 9999) + " " +randomFromInterval(1000, 9999) + " " +randomFromInterval(1000, 9999) + " " +randomFromInterval(1000, 9999) + " " +
  randomFromInterval(1000, 9999);
  var balance_ini = 100;

  var newAccount = {
    //"account_id": req.body.account_id,
    //"IBAN": req.body.IBAN,
    //"balance": req.body.balance,
    "account_id": accountIdRand,
    "IBAN": ibanRand,
    "balance": balanceIni,
    "alias":  req.body.alias,
    "user_id":  req.body.user_id,
    "entry_date": entryDate
  };
  /**
  var newAccount = {
    "account_id": 12,
    "IBAN": "FR25 1963 1580 83WY YLVW W0GX K87",
    "balance": 368,
    "alias": "adipiscing elit proin interdum",
    "user_id": 1
  };
  **/
  var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Creando cuenta... ");
    httpClient.post("account?" + mLabAPIKey, newAccount,
      function(err, resMLab, body) {
        var response =!err ? body : {
          "msg" : "Error en creacion de Cuenta"
          }
          res.send(response);
    //console.log(body);
    }
  );
}


//EXPORTS DE LAS LLAMADAS
module.exports.getAccounts = getAccounts;
module.exports.createAccounts = createAccounts;
