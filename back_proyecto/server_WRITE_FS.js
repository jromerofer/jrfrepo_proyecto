//APITechU JRF

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser =require('body-parser'); // libreria para utilizar body en la API

app.use(bodyParser.json());
app.listen(port); // Levantamos el servidor para escuchar en un puerto
console.log("API escuchando en el puerto: " + port + " BBBEEEEEEPPP");

const userController = require('./controller/userController');
const authController = require('./controller/AuthController');

// GET USERS
app.get('/apitechu/v1/users', userController.getUsersV1); //userController.getUsersV1 es la variable que indica la funcion.
// POST USERS
app.post('/apitechu/v1/users', userController.createUsersV1);
app.post('/apitechu/v2/users', userController.createUsersV2);
// POST DELETE
app.delete("/apitechu/v1/users/:id", userController.deleteUsersV1);
//LOGIN
app.post("/apitechu/v1/users/login", authController.loginV1);
//LOGOUT
app.post("/apitechu/v1/users/logout", authController.logoutV1);

//GET USERS V2
app.get('/apitechu/v2/users', userController.getUsersV2);
//GET USERS V2 BY ID
app.get('/apitechu/v2/users/:id', userController.getUsersByV2);


//**********************************************//
// GET HELLO registro de ruta de API
app.get('/apitechu/v1/hello',
  function(req, res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde APITechU !!"});
  }
);

//POST MONSTRUO
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
   console.log("POST /apitechu/v1/monstruo/:p1/p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);
